package com.tmh.dailynamaz

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import com.tmh.dailynamaz.Utils.Companion.PERMISSION_REQUEST_ACCESS_FINE_LOCATION
import com.tmh.dailynamaz.Utils.Companion.isOnline
import com.tmh.dailynamaz.Utils.Companion.setLogs
import com.tmh.dailynamaz.api.RetrofitClient
import com.tmh.dailynamaz.model.TimeMain
import kotlinx.android.synthetic.main.activity_intro.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.slider_item_container.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class IntroActivity : AppCompatActivity() {

   // lateinit var recyclerAdapter: RecyclerviewItemAdapter

    private val introSliderAdapter = IntroSliderAdapter(
        listOf(
            IntroSlide(
                "Privacy Focused",
                "sunlight is the light and energy that comes from the Sun.",
                R.drawable.ic_intro_onee
            ),
            IntroSlide(
                "Allow Location",
                "Electronic bill payment is a feature of online, mobile and telephone banking.",
                R.drawable.ic_intro_two
            )
        )
    )


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)

       // recyclerAdapter = RecyclerviewItemAdapter(this)

        introSliderViewPager.adapter = introSliderAdapter
        setupIndicators()
        setCurrentIndicator(0)
        introSliderViewPager.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if(position == 1){
                    getLocation()
                }
                setCurrentIndicator(position)
            }
        })

        buttonNext.setOnClickListener {
            if (introSliderViewPager.currentItem + 1 < introSliderAdapter.itemCount) {
                introSliderViewPager.currentItem += 1
            } else {
                if(isOnline(this@IntroActivity)){
                    getLocation()
                }else{
                    Toast.makeText(applicationContext, "No Internet", Toast.LENGTH_SHORT).show()
                }

            }
        }

        textSkipIntro.setOnClickListener {
            Intent(applicationContext, MainActivity::class.java).also {
                startActivity(it)
                finish()
            }
        }

    }

    private fun setupIndicators() {
        val indicators = arrayOfNulls<ImageView>(introSliderAdapter.itemCount)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        layoutParams.setMargins(8, 0, 8, 0)

        for (i in indicators.indices) {
            indicators[i] = ImageView(applicationContext)
            indicators[i].apply {
                this?.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_inactive
                    )

                )
                this?.layoutParams = layoutParams
            }

            indicatorContainer.addView(indicators[i])
        }
    }

    private fun setCurrentIndicator(index: Int) {
        val chilCount = indicatorContainer.childCount
        for (i in 0 until chilCount) {
            val imageView = indicatorContainer[i] as ImageView
            if (i == index) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_active
                    )
                )
            } else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_inactive
                    )
                )
            }
        }
    }

    fun getLocation() {
        setLogs("Location permission allowed")
        var locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?

        var locationListener = object : LocationListener {

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onLocationChanged(location: Location) {
                var latitute = location.latitude
                var longitute = location.longitude
                Log.i("test", "Latitute: $latitute ; Longitute: $longitute")

                if(isOnline(this@IntroActivity)){
                    textTitle.text = "Please wait..."
                    textDescription.text = "We are synching Namaz timing based on location"
                    indicatorContainer.visibility = View.GONE
                    buttonNext.visibility = View.GONE
                    serverTimeLocally(latitute, longitute,"1", Utils.getCurrentMonth(), Utils.getCurrentYear())
                }else{
                    setLogs("No Internet")
                    Toast.makeText(applicationContext, "No Internet", Toast.LENGTH_SHORT).show()
                }


            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            }


        }

        try {
            locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 50000f, locationListener)
        } catch (ex:SecurityException) {
            //Toast.makeText(applicationContext, "caught some exception", Toast.LENGTH_SHORT).show()
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSION_REQUEST_ACCESS_FINE_LOCATION)
            return
        }
        locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 500000f, locationListener)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_ACCESS_FINE_LOCATION) {
            when (grantResults[0]) {
                PackageManager.PERMISSION_GRANTED -> getLocation()
                PackageManager.PERMISSION_DENIED ->  getMandatoryPermission()//Tell to user the need of grant permission
            }
        }
    }

    private fun getMandatoryPermission() {
        setLogs("Location Permission Deny")
        Toast.makeText(applicationContext, "Please allow location", Toast.LENGTH_SHORT).show()
        getLocation()
        setLogs("ReApply Location Permission")
    }

    fun serverTimeLocally(latitude:Double, longitude:Double, method:String, month: String, year:String) {
        RetrofitClient.instance.getTime(latitude, longitude,method,month, year)
                .enqueue(object : Callback<TimeMain> {
                    override fun onResponse(call: Call<TimeMain>, response: Response<TimeMain>) {
                        setLogs("RESPONSE ${response.body()}")
                        response.body()?.let { it ->
                            // recyclerAdapter.setMovieListItems(it?.data)
                            Utils.saveData(it.data, this@IntroActivity)

                            textTitle.text = "NAMAZ TIME FETCHED"
                            textDescription.text = "Please click below button to go home screen"
                            buttonNext.visibility = View.VISIBLE

                            // ALL PARAMETERS NEED TO DISPLAY
                            Log.d("i", "" + response.body()!!.data.get(0).date.readable) // date
                            Log.d("i", "" + response.body()!!.data.get(0).date.hijri.month.en) // "Muḥarram"
                            Log.d("i", "" + response.body()!!.data.get(0).date.hijri.month.ar) // ""مُحَرَ" "
                            Log.d("i", "" + response.body()!!.data.get(0).date.hijri.year) // "1443"
                            Log.d("i", "" + response.body()!!.data.get(0).meta.latitude) // lat
                            Log.d("i", "" + response.body()!!.data.get(0).meta.longitude) // lon
                            Log.d("i", "" + response.body()!!.data.get(0).timings.Fajr)
                            Log.d("i", "" + response.body()!!.data.get(0).timings.Dhuhr)
                            Log.d("i", "" + response.body()!!.data.get(0).timings.Asr)
                            Log.d("i", "" + response.body()!!.data.get(0).timings.Maghrib)
                            Log.d("i", "" + response.body()!!.data.get(0).timings.Isha)

                            Intent(applicationContext, MainActivity::class.java).also {
                                startActivity(it)
                                finish()
                                setLogs("Data fetched successfully going to MainScreen")
                            }

                        }
                    }

                    override fun onFailure(call: Call<TimeMain>, t: Throwable) {
                        buttonNext.isEnabled = false
                        setLogs("onFailure : ${t.message}")
                    }

                })
    }

}