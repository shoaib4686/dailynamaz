package com.tmh.dailynamaz

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.location.Address
import android.location.Geocoder
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import com.tmh.dailynamaz.model.Data
import java.text.SimpleDateFormat
import java.util.*


class Utils {

    companion object {

        /* CONSTANT */
         const val PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 100
         const val SPLASH_TIMER:Long = 2000
         const val INTRO_DISPLAYED : String = "DISPLAYED"
         const val APP_TAG : String = "DAILYNAMAZ"
         enum class Namaz{ FAJR, DHUHR, ASR,MAGRIB,ISHA }


        // get current year、month and day
        var calendar = Calendar.getInstance()
        var year = calendar[Calendar.YEAR]
        var month = calendar[Calendar.MONTH]
        var day = calendar[Calendar.DATE]

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun isOnline(context: Context): Boolean {
            val connectivityManager =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivityManager != null) {
                val capabilities =
                        connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                        return true
                    }
                }
            }
            return false
        }

        fun getCurrentDate():String{
            val sdf = SimpleDateFormat("dd-MM-yyyy") //yyyy-MM-dd'T'HH:mm:ss.SSS
            return sdf.format(Date())
        }

        fun getCurrentMonth(): String {
            val sdf = SimpleDateFormat("MM") //yyyy-MM-dd'T'HH:mm:ss.SSS
            return sdf.format(Date())
        }

        fun getCurrentYear(): String {
            val sdf = SimpleDateFormat("yyyy") //yyyy-MM-dd'T'HH:mm:ss.SSS
            return sdf.format(Date())
        }

        fun getCurrentTime(): String {
            val sdf = SimpleDateFormat("HH:mm:ss") //yyyy-MM-dd'T'HH:mm:ss.SSS
            return sdf.format(Date())
        }

        fun setLogs(action_name:String){
            Log.d(APP_TAG,action_name)
        }

        fun getCityName(context: Context,MyLat:Double,MyLong:Double): String {
            var geocoder = Geocoder(context, Locale.getDefault())
            var addresses: List<Address> = geocoder.getFromLocation(MyLat, MyLong, 1)
            var cityName: String = addresses[0].getAddressLine(0)
            //var stateName: String = addresses[0].getAddressLine(1)
            //var countryName: String = addresses[0].getAddressLine(2)

            return cityName

        }


        fun saveObjectToArrayList(timeList: List<Data>) {

            Log.d("--list", "${timeList}")
            Log.d("--list", "${timeList.get(0)}")
            Log.d("--list", "${timeList.size}")
            //val jsonCars: String = Gson().toJson(example);

           //  val names = listOf("Anne", "Peter", "Jeff")
            for (name in timeList) {
               // Log.d("--list", "${name}")
                println(name)
            }
        }


         fun saveData(timeList: List<Data>, context: Context) {
            // method for saving the data in array list.
            // creating a variable for storing data in
            // shared preferences.
            val sharedPreferences: SharedPreferences = context.getSharedPreferences("shared preferences", MODE_PRIVATE)

            // creating a variable for editor to
            // store data in shared preferences.
            val editor = sharedPreferences.edit()

            // creating a new variable for gson.
            val gson = Gson()

            // getting data from gson and storing it in a string.
            val json = gson.toJson(timeList)

            // below line is to save data in shared
            // prefs in the form of string.
            editor.putString("courses", json)

            // below line is to apply changes
            // and save data in shared prefs.
            editor.apply()

            // after saving data we are displaying a toast message.
          //  Toast.makeText(context, "Saved Array List to Shared preferences. ", Toast.LENGTH_SHORT).show()
        }






    }

    //Other Refernces
    //https://github.com/velmurugan-murugesan/Android-Example
    //https://tutorials.eu/android-recyclerview-in-kotlin/
    //https://www.geeksforgeeks.org/how-to-save-arraylist-to-sharedpreferences-in-android/


    // Learning
    /*"it" val list = listOf(1,2,3,4,5,6,7).filter { it > 2 }
    for(i in list){
        println(i)
    }
    In this Example, list size is 7. Using it, list size is 5. for loop only check > 2 numbers
    size = 5 and output is 3,4,5,6,7
    */

    /*AlertDialog.Builder(this).apply {
            setTitle("Please confirm.")
            setMessage("Are you want to exit the app?")

            setPositiveButton("Yes") { _, _ ->
                // if user press yes, then finish the current activity
                super.onBackPressed()
            }

            setNegativeButton("No"){_, _ ->
                // if user press no, then return the activity
                Toast.makeText(this@MainActivity, "Thank you",
                        Toast.LENGTH_LONG).show()
            }

            setCancelable(true)
        }.create().show()*/


}
