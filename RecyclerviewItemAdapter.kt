package com.tmh.dailynamaz

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tmh.dailynamaz.Utils.Companion.saveData
import com.tmh.dailynamaz.model.Data

class RecyclerviewItemAdapter(val context: Context) :
    RecyclerView.Adapter<RecyclerviewItemAdapter.MyViewHolder>() {

    var timeList : List<Data> = listOf()

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(com.tmh.dailynamaz.R.layout.item_row, parent, false)
        return MyViewHolder(view)
    }


    //this method is binding the data on the list
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = timeList.get(position).timings.Fajr
        holder.tvName.setText("Namaz time is ${item}")
       // Glide.with(context).load(movieList.get(position).image)
        //                .apply(RequestOptions().centerCrop())
        //                .into(holder.image)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return timeList.size;
    }

    //the class is holding the list view
    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        val tvName: TextView = itemView!!.findViewById(com.tmh.dailynamaz.R.id.tvName)
        //   val image: ImageView = itemView!!.findViewById(R.id.image)
    }

    fun setMovieListItems(timeList: List<Data>){
        this.timeList = timeList;
        notifyDataSetChanged()
       // saveObjectToArrayList(timeList)
          saveData(timeList,context)
    }



}