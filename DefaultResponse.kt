package com.tmh.dailynamaz

import com.tmh.dailynamaz.model.Data

data class DefaultResponse(
    var error:Boolean,
    val message:String,
)