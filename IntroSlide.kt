package com.tmh.dailynamaz

data class IntroSlide(val title: String, val description: String, val icon: Int)