package com.tmh.dailynamaz

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.tmh.dailynamaz.Utils.Companion.INTRO_DISPLAYED
import com.tmh.dailynamaz.Utils.Companion.getCityName
import com.tmh.dailynamaz.Utils.Companion.getCurrentDate
import com.tmh.dailynamaz.Utils.Companion.getCurrentMonth
import com.tmh.dailynamaz.Utils.Companion.getCurrentTime
import com.tmh.dailynamaz.Utils.Companion.getCurrentYear
import com.tmh.dailynamaz.Utils.Companion.isOnline
import com.tmh.dailynamaz.Utils.Companion.setLogs
import com.tmh.dailynamaz.api.RetrofitClient
import com.tmh.dailynamaz.model.Data
import com.tmh.dailynamaz.model.TimeMain
import com.tmh.dailynamaz.sharedPreference.IPreferenceHelper
import com.tmh.dailynamaz.sharedPreference.PreferenceManager
import kotlinx.android.synthetic.main.activity_intro.*
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.util.*

class MainActivity : AppCompatActivity() {

    /* Time Algorithm
    Step 1. Take Current Date
    Step 2. Pass Current Date and Get relevant Timing info
    Step 3.
    */

    /* SYNCHING ALGORITHM
     Step 1. Take last index date
     Step 2. Compare last date and call start synching
    */

    private var pressedTime: Long = 0
    lateinit var recyclerView: RecyclerView
    lateinit var recyclerAdapter: RecyclerviewItemAdapter
    private val preferenceHelper: IPreferenceHelper by lazy { PreferenceManager(applicationContext) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recycleView)
        recyclerAdapter = RecyclerviewItemAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = recyclerAdapter
        tv_time.setText(getCurrentTime())

        preferenceHelper.setApiKey(INTRO_DISPLAYED)

        setBackground()

        getLocation()
    }

    private fun setBackground() {
        val c: Calendar = Calendar.getInstance()
        val timeOfDay: Int = c.get(Calendar.HOUR_OF_DAY)

        if (timeOfDay in 0..6) {
            rlayout.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_fajr));
            img_bg.setBackgroundResource(R.drawable.ic_bg_center_isha)
            setLogs("Apply Fajr Background");
            applyTimeBg(Utils.Companion.Namaz.FAJR.toString())

        } else if (timeOfDay in 7..15) {
            rlayout.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_dhuhr));
            img_bg.setBackgroundResource(R.drawable.ic_bg_center_dhuhr)
            setLogs("Apply Dhuhr Background");
            applyTimeBg(Utils.Companion.Namaz.DHUHR.toString())

        } else if (timeOfDay in 16..17) {
            rlayout.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_asr));
            img_bg.setBackgroundResource(R.drawable.ic_bg_center_dhuhr)
            setLogs("Apply Asr Background");
            applyTimeBg(Utils.Companion.Namaz.ASR.toString())

        } else if (timeOfDay in 18..19) {
            rlayout.background = ContextCompat.getDrawable(this, R.drawable.bg_magrib);
            img_bg.setBackgroundResource(R.drawable.ic_bg_center_isha)
            setLogs("Apply Magrib Background");
            applyTimeBg(Utils.Companion.Namaz.MAGRIB.toString())

        } else if (timeOfDay in 20..23) {
            rlayout.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_isha));
            img_bg.setBackgroundResource(R.drawable.ic_bg_center_isha)
            setLogs("Apply Isha Background");
            applyTimeBg(Utils.Companion.Namaz.ISHA.toString())
        }
    }

    private fun applyTimeBg(option : String ) {
        when (option) {
            Utils.Companion.Namaz.FAJR.toString() -> {
                ll_select_time_fajr.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_time));
                ll_select_time_dhuhr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_asr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_magrib.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_isha.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
            }
            Utils.Companion.Namaz.DHUHR.toString() -> {
                ll_select_time_fajr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_dhuhr.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_time));
                ll_select_time_asr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_magrib.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_isha.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));

            }
            Utils.Companion.Namaz.ASR.toString() -> {
                ll_select_time_fajr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_dhuhr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_asr.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_time));
                ll_select_time_magrib.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_isha.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));

            }
            Utils.Companion.Namaz.MAGRIB.toString() -> {
                ll_select_time_fajr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_dhuhr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_asr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_magrib.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_time));
                ll_select_time_isha.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));

            }
            Utils.Companion.Namaz.ISHA.toString() -> {
                ll_select_time_fajr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_dhuhr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_asr.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_magrib.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_all_time));
                ll_select_time_isha.setBackground(ContextCompat.getDrawable(this, R.drawable.selected_time));

            }
            else -> {
                println("\nInvalid option!\n")
            }
        }
     }

    private fun getLocation() {

        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?

        val locationListener = object : LocationListener {

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onLocationChanged(location: Location) {

                val latitute = location!!.latitude
                val longitute = location!!.longitude

                if (isOnline(this@MainActivity)) {
                    setLogs("GetCityName ${getCityName(this@MainActivity, latitute, longitute)}");
                    tv_city.setText(getCityName(this@MainActivity, latitute, longitute))
                }else{
                    setLogs("LatLon ${latitute} - ${longitute}");
                }

                loadData(this@MainActivity, getCurrentDate(), latitute, longitute)
                setLogs("Passing current date, latlon and load data ")
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            }
        }

        try {
            locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 50000f, locationListener)
        } catch (ex: SecurityException) {
            setLogs("locationManager : ")
        }
    }


    fun loadData(context: Context, currentDate: String, latitute: Double, longitute: Double) {
        // method to load arraylist from shared prefs
        // initializing our shared prefs with name as
        // shared preferences.
        val courseModalArrayList: ArrayList<Data>

        val sharedPreferences: SharedPreferences = context.getSharedPreferences("shared preferences", MODE_PRIVATE)

        // creating a variable for gson.
        val gson = Gson()

        // below line is to get to string present from our
        // shared prefs if not present setting it as null.
        val json = sharedPreferences.getString("courses", null)

        // below line is to get the type of our array list.
        val type: Type = object : TypeToken<ArrayList<Data?>?>() {}.type

        // in below line we are getting data from gson
        // and saving it to our array list
        courseModalArrayList = gson.fromJson<Any>(json, type) as ArrayList<Data>

        // checking below if the array list is empty or not
        if (courseModalArrayList != null) {
            //courseModalArrayList = ArrayList()
            for (item in courseModalArrayList.indices) {
                // Log.d("--list", "courseModalArrayList[$item]:") // for index
                val iCheck = "$item".toInt()
                setLogs("Checking current date in Namaz database ${courseModalArrayList[iCheck].date.gregorian.date}")
                if (currentDate == courseModalArrayList[iCheck].date.gregorian.date) {
                    tv_fajr.setText(courseModalArrayList[iCheck].timings.Fajr)
                    tv_dhuhr.setText(courseModalArrayList[iCheck].timings.Dhuhr)
                    tv_asr.setText(courseModalArrayList[iCheck].timings.Asr)
                    tv_magrib.setText(courseModalArrayList[iCheck].timings.Maghrib)
                    tv_isha.setText(courseModalArrayList[iCheck].timings.Isha)
                } else {
                    setLogs("Fetching new month data") // Fetch new month data
                    serverTimeLocally(latitute, longitute, "1", Utils.getCurrentMonth(), Utils.getCurrentYear())
                }
            }
        }else{
            setLogs("List is empty :${courseModalArrayList}")
        }
    }


    private fun serverTimeLocally(latitude: Double, longitude: Double, method: String, month: String, year: String) {
        RetrofitClient.instance.getTime(latitude, longitude, method, month, year)
                .enqueue(object : Callback<TimeMain> {
                    override fun onResponse(call: Call<TimeMain>, response: Response<TimeMain>) {
                        Log.d("--", "" + response.body())
                        response.body()?.let {
                            Utils.saveData(it.data, this@MainActivity)
                            setLogs("onResponse : ${it.data}")
                        }
                    }

                    override fun onFailure(call: Call<TimeMain>, t: Throwable) {
                        Log.d("FetchTime", "" + t.message)
                        setLogs("onResponse : ${t.message}")
                    }

                })
    }

    override fun onBackPressed() {
        if (pressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            startActivity(Intent(this, T::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or
                        Intent.FLAG_ACTIVITY_NEW_TASK)
                setLogs("Backpressed clicked")
            })
            // finish();
        } else {
            Toast.makeText(getBaseContext(), "Press back again to exit", Toast.LENGTH_SHORT).show();
        }
        pressedTime = System.currentTimeMillis();
    }


}





