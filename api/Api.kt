package com.tmh.dailynamaz.api

import com.tmh.dailynamaz.model.TimeMain
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    //calendar?latitude=51.508515&longitude=-0.1254872&method=2&month=4&year=2017
    //URL:- calendarByCity?city=Durg&country=IND&method=1&month=11&year=2021
    //fun getTime() : Call<TimeMain>

    @GET("calendar?")
    fun getTime(@Query("latitude") latitude: Double,
                @Query("longitude") longitude: Double,
                @Query("method") method: String,
                @Query("month") month: String,
                @Query("year") year: String) : Call<TimeMain>
}