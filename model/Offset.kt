package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName

   
data class Offset (

   @SerializedName("Imsak") var Imsak : Int,
   @SerializedName("Fajr") var Fajr : Int,
   @SerializedName("Sunrise") var Sunrise : Int,
   @SerializedName("Dhuhr") var Dhuhr : Int,
   @SerializedName("Asr") var Asr : Int,
   @SerializedName("Maghrib") var Maghrib : Int,
   @SerializedName("Sunset") var Sunset : Int,
   @SerializedName("Isha") var Isha : Int,
   @SerializedName("Midnight") var Midnight : Int

)