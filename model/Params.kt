package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName

   
data class Params (

   @SerializedName("Fajr") var Fajr : Int,
   @SerializedName("Isha") var Isha : Int

)