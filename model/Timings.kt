package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName

   
data class Timings (

   @SerializedName("Fajr") var Fajr : String,
   @SerializedName("Sunrise") var Sunrise : String,
   @SerializedName("Dhuhr") var Dhuhr : String,
   @SerializedName("Asr") var Asr : String,
   @SerializedName("Sunset") var Sunset : String,
   @SerializedName("Maghrib") var Maghrib : String,
   @SerializedName("Isha") var Isha : String,
   @SerializedName("Imsak") var Imsak : String,
   @SerializedName("Midnight") var Midnight : String

)