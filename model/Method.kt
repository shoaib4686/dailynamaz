package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName

   
data class Method (

   @SerializedName("id") var id : Int,
   @SerializedName("name") var name : String,
   @SerializedName("params") var params : Params,
   @SerializedName("location") var location : Location

)