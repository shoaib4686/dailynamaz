package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName

   
data class Location (

   @SerializedName("latitude") var latitude : Double,
   @SerializedName("longitude") var longitude : Double

)