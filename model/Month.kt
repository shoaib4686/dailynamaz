package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName

   
data class Month (

   @SerializedName("number") var number : Int,
   @SerializedName("en") var en : String,
   @SerializedName("ar") var ar : String

)