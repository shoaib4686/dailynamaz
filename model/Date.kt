package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName

   
data class Date (

   @SerializedName("readable") var readable : String,
   @SerializedName("timestamp") var timestamp : String,
   @SerializedName("gregorian") var gregorian : Gregorian,
   @SerializedName("hijri") var hijri : Hijri

)