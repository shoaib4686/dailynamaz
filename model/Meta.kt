package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName

   
data class Meta (

   @SerializedName("latitude") var latitude : Double,
   @SerializedName("longitude") var longitude : Double,
   @SerializedName("timezone") var timezone : String,
   @SerializedName("method") var method : Method,
   @SerializedName("latitudeAdjustmentMethod") var latitudeAdjustmentMethod : String,
   @SerializedName("midnightMode") var midnightMode : String,
   @SerializedName("school") var school : String,
   @SerializedName("offset") var offset : Offset

)