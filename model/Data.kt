package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName

   
data class Data (

   @SerializedName("timings") var timings : Timings,
   @SerializedName("date") var date : Date,
   @SerializedName("meta") var meta : Meta

)