package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName


data class Hijri (

   @SerializedName("date") var date : String,
   @SerializedName("format") var format : String,
   @SerializedName("day") var day : String,
   @SerializedName("weekday") var weekday : Weekday,
   @SerializedName("month") var month : Month,
   @SerializedName("year") var year : String,
   @SerializedName("designation") var designation : Designation,
   @SerializedName("holidays") var holidays : List<String>

)