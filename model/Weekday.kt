package com.tmh.dailynamaz.model

import com.google.gson.annotations.SerializedName

   
data class Weekday (

   @SerializedName("en") var en : String,
   @SerializedName("ar") var ar : String

)