package com.tmh.dailynamaz

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tmh.dailynamaz.Utils.Companion.INTRO_DISPLAYED
import com.tmh.dailynamaz.Utils.Companion.SPLASH_TIMER
import com.tmh.dailynamaz.Utils.Companion.setLogs
import com.tmh.dailynamaz.sharedPreference.IPreferenceHelper
import com.tmh.dailynamaz.sharedPreference.PreferenceManager
import java.util.*


class SplashActivity : AppCompatActivity() {

    private val preferenceHelper: IPreferenceHelper by lazy { PreferenceManager(applicationContext) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        splashTimer() // SPLASH TIMER
    }

    private fun splashTimer(){
        Timer().schedule(object : TimerTask() {
            override fun run() {
                if(preferenceHelper.getApiKey() == INTRO_DISPLAYED){
                    setLogs("Intro Displayed");
                    startActivity(Intent(applicationContext,  MainActivity::class.java))
                }else{
                    setLogs("View Intro slide");
                    startActivity(Intent(applicationContext, IntroActivity::class.java))
                }
            }
        }, SPLASH_TIMER)
    }

}